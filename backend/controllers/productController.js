import Product from '../models/productModel.js';
import asyncHandler from 'express-async-handler';

// @desc Fetch all products
// @route GET /api/products
// @access Public
const getProducts = asyncHandler(async (req, res) => {
  const pageSize = 6;
  const page = Number(req.query.pageNumber) || 1;
  // USING REGEX IN ORDER TTO BE ABLE TO SEARCH WITH PART OF NAME
  // OPTIONS I MEANS CASE INSENSITIVE
  const keyword = req.query.keyword
    ? { name: { $regex: req.query.keyword, $options: 'i' } }
    : {};

  const count = await Product.countDocuments({ ...keyword });
  const products = await Product.find({ ...keyword })
    .limit(pageSize)
    .skip(pageSize * (page - 1));
  res.json({ products, page, pages: Math.ceil(count / pageSize) });
});

// @desc Fetch single product
// @route GET /api/products/:id
// @access Public
const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    res.json(product);
  } else {
    res.status(404);
    throw new Error('Product not found');
  }
});

// @desc Delete product
// @route DELETE /api/products/:id
// @access Private/Admin
const deleteProduct = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);
  if (product) {
    await Product.deleteOne({ _id: req.params.id });
    res.json({ message: 'Product removed' });
  } else {
    res.status(404);
    throw new Error('Product not found');
  }
});

// @desc Create product
// @route POST /api/products
// @access Private/Admin
const createProduct = asyncHandler(async (req, res) => {
  const product = new Product({
    name: 'Sample name',
    price: 0,
    user: req.user._id,
    image: '/images/sample.jpg',
    brand: 'Sample Brand',
    category: 'Sample category',
    countInStock: 0,
    numReviews: 0,
    description: 'Sample description',
  });

  const createdProduct = await product.save();
  res.status(201).json(createdProduct);
});

// @desc Update product
// @route PUT /api/products/:id
// @access Private/Admin
const updateProduct = asyncHandler(async (req, res) => {
  // const { name, price, image, brand, category, countInStock, description } =
  //   req.body;

  const product = await Product.findById(req.params.id);

  if (product) {
    product.name = req.body.name;
    product.price = req.body.price;
    product.description = req.body.description;
    product.image = req.body.image;
    product.brand = req.body.brand;
    product.category = req.body.category;
    product.countInStock = req.body.countInStock;
    // product.user = req.body.user || product.user;
    // product.numReviews = req.body.numReviews || product.numReviews;

    const updatedProduct = await product.save();

    res.json(updatedProduct);
  } else {
    res.status(404);
    throw new Error('User could not be updated!');
  }
});

// @desc Create new review product
// @route POST /api/products/:id/reviews
// @access Private
const createProductReview = asyncHandler(async (req, res) => {
  const { rating, comment } = req.body;

  console.log('rating from req:', rating);

  const product = await Product.findById(req.params.id);

  if (product) {
    const existingReview = product.reviews.find(
      (r) => r.user.toString() === req.user._id.toString()
    );
    if (existingReview) {
      // existingReview.comment = req.body.comment;

      // let oldRating = existingReview.rating;
      // let newRating = req.body.rating;

      // let productSumOfRatings = product.numReviews * product.rating;
      // product.rating = Number(
      //   (productSumOfRatings - oldRating + newRating) / product.numReviews
      // );

      // existingReview.rating = req.body.rating;

      // await product.save();
      // res.status(201);
      // THIS IS IF WE DO NOT WANT TO UPDATE THE REVIEW
      res.status(400).json({ message: 'Already reviewed' });
      throw new Error('Product already reviewed');
    }
    const review = {
      name: req.user.name,
      rating: Number(rating),
      comment,
      user: req.user._id,
    };

    let productSumOfRatings = product.numReviews * product.rating;

    product.reviews.push(review);
    product.numReviews = product.reviews.length;

    product.rating = Number(
      (productSumOfRatings + review.rating) / product.numReviews
    );

    // product.rating =
    //   product.reviews.reduce((acc, item) => item.rating + acc.Error, 0) /
    //   product.reviews.length;

    let updatedProduct = await product.save();
    res
      .status(201)
      .json({ updatedProduct: updatedProduct, message: 'Review added' });
  } else {
    res.status(404);
    throw new Error('Product could not be updated!');
  }
});

// @desc Get top rated products
// @route GET /api/products/top
// @access Public
const getTopProducts = asyncHandler(async (req, res) => {
  const carouselLimit = 3;

  const topProducts = await Product.find({})
    .sort({ rating: -1 })
    .limit(carouselLimit);
  res.json(topProducts);
});

export {
  getProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateProduct,
  createProductReview,
  getTopProducts,
};
