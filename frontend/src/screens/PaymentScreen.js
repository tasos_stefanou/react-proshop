import React, { useState } from 'react';
import { Button, Form, Col, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import FormContainer from '../components/FormContainer';
import CheckoutSteps from '../components/CheckoutSteps';
import { savePaymentMethod } from '../actions/cartActions';

const PaymentScreen = ({ history }) => {
  const cart = useSelector((state) => state.cart);
  const { shippingAddress } = cart;

  if (!shippingAddress) {
    history.push('/shipping');
  }
  //   !shippingAddress && history.push('/shipping');

  const [paymentMethod, setPaymentMethod] = useState('');

  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(savePaymentMethod(paymentMethod));
    history.push('/placeorder');
  };

  return (
    <FormContainer>
      <CheckoutSteps step1 step2 step3 />
      <h1>Payment Method</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group className='py-3'>
          <Form>
            <Form.Label as='legend'>Select Method</Form.Label>
          </Form>
          <Col>
            <Row>
              <Form.Check
                className='py-3'
                type='radio'
                label='Paypal'
                id='PayPal'
                name='paymentmethod'
                value='PayPal'
                onChange={(e) => setPaymentMethod(e.target.value)}
              ></Form.Check>
            </Row>
            <Row>
              <Form.Check
                className='py-3'
                type='radio'
                label='Stripe'
                id='Stripe'
                name='paymentmethod'
                value='Stripe'
                onChange={(e) => setPaymentMethod(e.target.value)}
              ></Form.Check>
            </Row>
            <Row>
              <Form.Check
                className='py-3'
                type='radio'
                label='Credit Card'
                id='CreditCard'
                name='paymentmethod'
                value='Credit Card'
                onChange={(e) => setPaymentMethod(e.target.value)}
              ></Form.Check>
            </Row>
          </Col>
        </Form.Group>

        <Button type='submit' variant='primary'>
          Continue
        </Button>
      </Form>
    </FormContainer>
  );
};

export default PaymentScreen;
